package org.geekbang.thinking.in.spring.bean.definition;

import org.geekbang.thinking.in.spring.bean.factory.UserFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Bean 垃圾回收示例
 * Spring 上下文被关闭且调用GC方法才会回收对象
 */
public class BeanGarbageCollectionDemo {

    public static void main(String[] args) throws InterruptedException {
        //创建 BeanFactory 容器
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        //注册 Configuration Class (配置类)
        applicationContext.register(BeanInitiallizationDemo.class);

        //启动 Spring 应用上下文(XML配置方式不需要)
        applicationContext.refresh();
        //显式的关闭 Spring 应用上下文
        applicationContext.close();
        System.out.println("Spring 上下文已关闭...");
        //强制GC
        System.gc();
        Thread.sleep(5000L);
    }
}
