package org.geekbang.thinking.in.spring.bean.definition;

import org.geekbang.thinking.in.spring.ioc.overview.domain.User;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Bean 别名示例
 */
public class BeanAliasDemo {

    public static void main(String[] args) {
        //配置 XML 配置文件
        //启动 Spring 应用上下文
        BeanFactory beanFactory = new ClassPathXmlApplicationContext("classpath:/META-INF/bean-definition-context.xml");
        //通过别名 baiyunlong-user 获取 User 的 Bean
        User baiyunlongUser = beanFactory.getBean("baiyunlong-user", User.class);
        User user = beanFactory.getBean("user", User.class);

        System.out.println("baiyunlong-user 是否与 user 相同 "+(user == baiyunlongUser));
    }
}
