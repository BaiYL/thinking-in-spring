package org.geekbang.thinking.in.spring.bean.definition;

import org.geekbang.thinking.in.spring.ioc.overview.domain.User;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Bean 实例化示例
 */
public class BeanInstantiationDemo {

    public static void main(String[] args) {
        //配置 XML 配置文件
        //启动 Spring 应用上下文
        ApplicationContext beanFactory = new ClassPathXmlApplicationContext("classpath:/META-INF/bean-instantiation-context.xml");
        User staticUser = beanFactory.getBean("user-by-static-method", User.class);
        User instanceUser = beanFactory.getBean("user-by-instance-method", User.class);
        User factoryBeanUser = beanFactory.getBean("user-by-factory-bean", User.class);
        System.out.println(staticUser);
        System.out.println(instanceUser);
        System.out.println(factoryBeanUser);
        System.out.println(staticUser == instanceUser);
        //通过Xml配置Spring应用上下文不需要refresh即可获取对象
        System.out.println("User 类型的所有 Beans "+beanFactory.getBeansOfType(User.class));
    }
}
