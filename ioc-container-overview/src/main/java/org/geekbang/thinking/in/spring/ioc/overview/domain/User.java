package org.geekbang.thinking.in.spring.ioc.overview.domain;

import lombok.Data;
import lombok.ToString;
import org.geekbang.thinking.in.spring.ioc.overview.enums.City;
import org.springframework.core.io.Resource;

import java.util.List;

@Data
@ToString
public class User {

    private Long id;

    private String name;

    private City city;

    private City[] workCities;

    private List<City> liveCities;

    private Resource configFileLocation;

    public static User createUser(){
        User user = new User();
        user.setId(1L);
        user.setName("白云龙");
        return user;
    }

}
