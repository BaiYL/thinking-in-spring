package org.geekbang.thinking.in.spring.ioc.overview.repository;

import lombok.Data;
import org.geekbang.thinking.in.spring.ioc.overview.domain.User;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.ObjectFactory;

import java.util.Collection;

/**
 * 用户信息仓库
 */
@Data
public class UserRepository {

    private Collection<User> users; //自定义 Bean

    private BeanFactory beanFactory;//内建非 Bean 对象（依赖）

    private ObjectFactory<User> userObjectFactory;
}
