package org.geekbang.thinking.in.spring.ioc.overview.domain;

import lombok.Data;
import org.geekbang.thinking.in.spring.ioc.overview.annotation.Super;

/**
 * 超级用户
 */
@Data
@Super
public class SuperUser extends User{

    private String address;
}
